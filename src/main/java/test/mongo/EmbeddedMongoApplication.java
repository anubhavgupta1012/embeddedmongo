package test.mongo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmbeddedMongoApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmbeddedMongoApplication.class, args);
	}

}
