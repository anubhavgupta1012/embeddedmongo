package test.mongo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import test.mongo.entity.CustomCollection;
import test.mongo.entity.NAME;
import test.mongo.pojo.Address;
import test.mongo.service.MongoService;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class TestController {

    @Autowired
    private MongoService mongoService;
    @Autowired
    private MongoTemplate mongoTemplate;

    @GetMapping("/")
    public String getString() {
        Map<String, List<String>> map = new HashMap<>();
        map.put("DG21159", Arrays.asList("A"));
        map.put("SK44468", Arrays.asList("a"));
        CustomCollection customCollection5 = new CustomCollection(NAME.ABC, map, new Address("testStreet", "tgestCity"));
        Map<String, List<String>> map1 = new HashMap<>();
        map1.put("DG21159", Arrays.asList("QWWJWB"));
        map1.put("SK44468", Arrays.asList("OOP", "ZZZ"));
        map1.put("SS91781", Arrays.asList("MMM"));
        CustomCollection customCollection2 = new CustomCollection(NAME.CDF, map1, new Address("testStreet", "tgestCity"));

        Map<String, List<String>> map2 = new HashMap<>();
        map2.put("DG21159", Arrays.asList("BBD"));
        map2.put("SK44468", Arrays.asList("YY"));
        CustomCollection customCollection = new CustomCollection(NAME.OPP, map2, new Address("testStreet", "tgestCity"));

        Map<String, List<String>> map3 = new HashMap<>();
        map3.put("DG21159", Arrays.asList("axe"));
        map3.put("SK44468", Arrays.asList("VV"));
        CustomCollection customCollection4 = new CustomCollection(NAME.HBBH, map3, new Address("testStreet", "tgestCity"));


        mongoService.createEntry(Arrays.asList(customCollection, customCollection2, customCollection5, customCollection4));
        return "created";
    }

    @GetMapping("/get")
    public List<CustomCollection> getData333() {
        List<CustomCollection> customCollections = mongoTemplate.findAll(CustomCollection.class);
        return customCollections;
    }


    @GetMapping("/get1")
    public List<CustomCollection> getData() {
        List<CustomCollection> customCollections = mongoTemplate.find(CustomCollection.getData1(), CustomCollection.class);
        return customCollections;
    }

    @GetMapping("/get2")
    public List<CustomCollection> getData2() {
        List<CustomCollection> customCollections = mongoTemplate.find(CustomCollection.getData2(), CustomCollection.class);
        return customCollections;
    }

    @GetMapping("/get3")
    public List<CustomCollection> getData3() {
        List<CustomCollection> customCollections = mongoTemplate.find(CustomCollection.getData3(), CustomCollection.class);
        return customCollections;
    }


    @GetMapping("/get4")
    public List<CustomCollection> getData4() {
        List<CustomCollection> customCollections = mongoTemplate.find(CustomCollection.getData4(), CustomCollection.class);
        return customCollections;
    }

    @GetMapping("/get5")
    public List<CustomCollection> getData5(@RequestParam String query) {
        List<CustomCollection> customCollections = mongoTemplate.find(CustomCollection.getData5(query), CustomCollection.class);
        return customCollections;
    }

    @GetMapping("/get6")
    public List<CustomCollection> getData6(@RequestParam String query) {
        List<CustomCollection> customCollections = mongoTemplate.find(CustomCollection.getData6(query), CustomCollection.class);
        return customCollections;
    }
}
