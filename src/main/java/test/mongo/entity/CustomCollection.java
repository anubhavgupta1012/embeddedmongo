package test.mongo.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import test.mongo.pojo.Address;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Document(collection = "CustomCollection")
public class CustomCollection {

    @Id
    private String id;
    private NAME name;
    private Map<String, List<String>> contacts;
    private Address address;

    public CustomCollection(NAME name, Map<String, List<String>> contacts, Address address) {
        this.name = name;
        this.contacts = contacts;
        this.address = address;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public NAME getName() {
        return name;
    }

    public void setName(NAME name) {
        this.name = name;
    }

    public Map<String, List<String>> getContacts() {
        return contacts;
    }

    public void setContacts(Map<String, List<String>> contacts) {
        this.contacts = contacts;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public static Query getData1() {
        return new Query().with(Sort.by(new Sort.Order(Sort.Direction.ASC, "contacts.DG21159")));
    }

    public static Query getData2() {
        return new Query().with(Sort.by(new Sort.Order(Sort.Direction.DESC, "contacts.DG21159")));
    }

    public static Query getData3() {
        return new Query().with(Sort.by(new Sort.Order(Sort.Direction.ASC, "contacts.SK44468")));
    }

    public static Query getData4() {
        return new Query().with(Sort.by(new Sort.Order(Sort.Direction.DESC, "contacts.SK44468")));
    }

    public static Query getData5(String query) {
        return new Query(Criteria.where("contacts.DG21159").regex(query, "i"));
    }

    public static Query getData6(String query) {
        return new Query(Criteria.where("contacts.SK44468").regex(query, "i"));
    }
}
