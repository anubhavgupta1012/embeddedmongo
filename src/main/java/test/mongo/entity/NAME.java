package test.mongo.entity;

public enum NAME {
    ABC,
    CDF,
    SSS,
    HBBH,
    OPP;
}
