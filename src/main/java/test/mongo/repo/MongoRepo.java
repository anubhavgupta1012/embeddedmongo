package test.mongo.repo;

import org.springframework.data.mongodb.repository.MongoRepository;
import test.mongo.entity.CustomCollection;

public interface MongoRepo extends MongoRepository<CustomCollection, String> {
}
