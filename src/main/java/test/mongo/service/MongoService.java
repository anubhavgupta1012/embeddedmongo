package test.mongo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import test.mongo.entity.CustomCollection;
import test.mongo.repo.MongoRepo;

import java.util.List;

@Service
public class MongoService {

    @Autowired
    private MongoRepo mongoRepo;

    public void createEntry(List<CustomCollection> collections) {
        mongoRepo.saveAll(collections);
    }
}
